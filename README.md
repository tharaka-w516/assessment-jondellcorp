## Deployment

Project is currently deployed at `https://accountmanagementapideployment.azurewebsites.net/`.
Blob trigger function to read the uploaded files and store the contents in Azure SQL database is deployed at `https://jondellaccounts.azurewebsites.net` function app.

## Unit tests

Unit tests are included for the service layer. Clone the repo to run the unit tests in dev environment.