using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Threading.Tasks;
using AccountDataProcessorFunction.Data;
using AccountDataProcessorFunction.Utilities;
using Microsoft.Azure.WebJobs;
using Microsoft.Extensions.Logging;
using OfficeOpenXml;

namespace AccountDataProcessorFunction
{
	public static class DataStoreFunction
    {
        [FunctionName("DataStoreFunction")]
        public static async Task Run([BlobTrigger("monthly-balances/{name}", Connection = "AzureWebJobsStorage")]Stream myBlob, string name, ILogger log)
        {
            log.LogInformation($"C# Blob trigger function Processed blob\n Name:{name} \n Size: {myBlob.Length} Bytes");

			try
			{
                if (name.EndsWith(".xlsx"))
                {
                    // Get year and month from file name
                    string formattedFileName = Path.GetFileNameWithoutExtension(name);
                    Utility.GetDateDetailsFromFileName(formattedFileName, out int year, out int month);

                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    using (ExcelPackage excelPackage = new ExcelPackage(myBlob))
                    {
                        List<AccountBalanceDto> accountBalances = new List<AccountBalanceDto>();
                        ExcelWorksheet excelWorksheet = excelPackage.Workbook.Worksheets[0];

                        for (int i = 2; i < excelWorksheet.Dimension.Rows + 1; i++)
                        {
                            string accountName = Convert.ToString(excelWorksheet.Cells[i, 1].Value).ToLower();
                            decimal balance = Convert.ToDecimal(excelWorksheet.Cells[i, 2].Value);

                            AccountBalanceDto accountBalance = new AccountBalanceDto
                            {
                                AccountId = GetAccountId(accountName),
                                Amount = balance,
                                Month = month,
                                Year = year
                            };
                            accountBalances.Add(accountBalance);
                        }

                        await InsertAccountDataToDatabase(accountBalances);
                    }
                }
            }
			catch (Exception ex)
			{
                log.LogError(ex, $"Something went wrong in DataStoreFunction: {ex.Message}");
				throw ex;
			}

        }

		#region Helper Methods

		private static async Task<int> InsertAccountDataToDatabase(List<AccountBalanceDto> accounts)
		{
			try
			{
                int result = 0;
                using (SqlConnection connection = new SqlConnection(Environment.GetEnvironmentVariable("SqlConnectionString")))
				{
                    connection.Open();
                    if (accounts != null && accounts.Count > 0)
					{
                        foreach (AccountBalanceDto balance in accounts)
						{
                            string query = $@"IF EXISTS (SELECT * FROM [dbo].[Balance] WHERE TransactionMonth={balance.Month} AND FinancialYear={balance.Year} AND AccountId={balance.AccountId})
                                                UPDATE [dbo].[Balance]
                                                SET Amount = {balance.Amount}
                                                WHERE TransactionMonth={balance.Month} AND FinancialYear={balance.Year} AND AccountId={balance.AccountId};
                                              ELSE
                                                INSERT INTO [dbo].[Balance] (Amount, TransactionMonth, FinancialYear, AccountId) VALUES({balance.Amount}, {balance.Month}, {balance.Year}, {balance.AccountId})";
                            SqlCommand command = new SqlCommand(query, connection);
                            result = await command.ExecuteNonQueryAsync();
                        }
                    }
                    connection.Close();
				}
                return result;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

        private static int GetAccountId(string account)
		{
			try
			{
                int accountId;

				switch (account)
				{
                    case "r&d":
                        accountId = 1;
                        break;
                    case "canteen":
                        accountId = 2;
                        break;
                    case "ceo's car":
                        accountId = 3;
                        break;
                    case "marketing":
                        accountId = 4;
                        break;
                    case "parking fines":
                        accountId = 5;
                        break;
					default:
                        accountId = 0;
						break;
				}

                return accountId;
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
