﻿using System;

namespace AccountDataProcessorFunction.Utilities
{
	public static class Utility
	{
		#region Public Methods

		public static void GetDateDetailsFromFileName(string fileName, out int year, out int month)
		{
			string[] splitFileName = fileName.Split('_');
			month = !string.IsNullOrEmpty(splitFileName[splitFileName.Length - 1]) ? Convert.ToInt32(splitFileName[splitFileName.Length - 1]) : 0;
			year = !string.IsNullOrEmpty(splitFileName[splitFileName.Length - 2]) ? Convert.ToInt32(splitFileName[splitFileName.Length - 2]) : 0;
		}

		#endregion
	}
}
