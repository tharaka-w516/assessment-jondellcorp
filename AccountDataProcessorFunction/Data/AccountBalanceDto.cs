﻿namespace AccountDataProcessorFunction.Data
{
	public class AccountBalanceDto
	{
		public int AccountId { get; set; }
		public decimal Amount { get; set; }
		public int Month { get; set; }
		public int Year { get; set; }
	}
}
