﻿using System.IO;
using System.Threading.Tasks;

namespace AccountManagementAPI.Services
{
	public interface IFileUploadService
	{
		Task<string> Create(Stream fileStream, string fileName, string contentType);
	}
}
