﻿using System.IO;
using System.Threading.Tasks;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace AccountManagementAPI.Services
{
	public class FileUploadService: IFileUploadService
	{
		#region Private Variables

		private static string blobConnectionString;
		private static string blobContainerName;
		private readonly ILogger _logger;

		#endregion

		#region Constructor

		public FileUploadService(IConfiguration configuration, ILogger<IFileUploadService> logger)
		{
			blobConnectionString = configuration.GetConnectionString("BlobStorageConnection");
			blobContainerName = configuration.GetValue<string>("BlobContainerName");
			_logger = logger;
		}

		#endregion

		#region Public Methods

		public async Task<string> Create(Stream fileStream, string fileName, string contentType)
		{
			BlobContainerClient blobContainer = new BlobContainerClient(blobConnectionString, blobContainerName);
			await blobContainer.CreateIfNotExistsAsync();

			BlobClient blobClient = blobContainer.GetBlobClient(fileName);
			await blobClient.UploadAsync(fileStream, new BlobHttpHeaders { ContentType = contentType });

			return blobClient.Uri.AbsoluteUri.ToString();
		}

		#endregion
	}
}
