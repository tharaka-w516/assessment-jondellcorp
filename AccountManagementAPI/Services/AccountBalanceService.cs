﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountManagementAPI.Data;
using AccountManagementAPI.Data.Dto;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace AccountManagementAPI.Services
{
	public class AccountBalanceService : IAccountBalanceService
	{
		#region Private Variables

		private readonly AccountContext _context;
		private readonly ILogger _logger;

		#endregion

		#region Constructor

		public AccountBalanceService(AccountContext context, ILogger<AccountBalanceService> logger)
		{
			_context = context;
			_logger = logger;
		}

		#endregion

		#region Public Methods

		public async Task<AccountCreationResponse> CreateAccount(string name)
		{
			AccountCreationResponse response = new AccountCreationResponse();

			try
			{
				if (string.IsNullOrEmpty(name))
					throw new Exception("Account name cannot be empty.");

				Account account = new Account
				{
					AccName = name,
					CreatedBy = "SystemUser",
					CreatedDate = DateTime.Now,
					ModifiedBy = "SystemUser",
					ModifiedDate = DateTime.Now
				};

				_context.Accounts.Add(account);
				int isCreated = await _context.SaveChangesAsync();

				if (isCreated == 1)
				{
					response.IsCreated = true;
					response.Account = account;
				}
			}
			catch (Exception ex)
			{
				string errorMessage = $"Something went wrong in CreateAccount: {ex.Message}";
				_logger.LogError(errorMessage);
				response.Message = errorMessage;
			}

			return response;
		}

		public async Task<AccountBalanceResponse> GetAccountBalancesByMonth(int accountId, int year, int month)
		{
			try
			{
				AccountBalanceResponse response = new AccountBalanceResponse();

				Account account = await _context.Accounts.Where(x => x.Id == accountId).FirstOrDefaultAsync();

				if (account == null)
					return new AccountBalanceResponse { Message = "Account not found." };

				List<Balance> queryResult = await _context.Balances
					.Where(x => x.AccountId == accountId && x.FinancialYear == year && x.TransactionMonth == month)
					.ToListAsync();

				if (queryResult == null || !queryResult.Any())
					return new AccountBalanceResponse { Message = "No account balances found." };

				response.IsFound = true;
				response.Balances = GetBalances(queryResult);

				return response;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetAccountBalanceByMonth: {ex.Message}");
				return null;
			}
		}

		public async Task<AccountBalanceResponse> GetAccountBalancesByYear(int accountId, int year)
		{
			try
			{
				AccountBalanceResponse accountBalance = new AccountBalanceResponse();

				Account account = await _context.Accounts.Where(x => x.Id == accountId).FirstOrDefaultAsync();

				if (account == null)
					return new AccountBalanceResponse { Message = "Account not found." };

				List<Balance> queryResult = await _context.Balances
					.Include(x => x.Account)
					.Where(x => x.AccountId == accountId && x.FinancialYear == year)
					.OrderBy(x => x.TransactionMonth)
					.ThenBy(x => x.AccountId)
					.ToListAsync();

				if (queryResult == null || !queryResult.Any())
					return new AccountBalanceResponse { Message = "No balances found." };

				accountBalance.IsFound = true;
				accountBalance.Balances = GetBalances(queryResult);

				return accountBalance;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetAccountBalanceByYear: {ex.Message}");
				return null;
			}
		}

		public async Task<List<Account>> GetAccounts()
		{
			try
			{
				List<Account> accounts = await _context.Accounts.Select(x => x).ToListAsync();

				return accounts;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetAccounts: {ex.Message}");
				return null;
			}
		}

		public async Task<AccountBalanceResponse> GetBalancesByAccountId(int id)
		{
			try
			{
				AccountBalanceResponse response = new AccountBalanceResponse();

				Account account = await _context.Accounts.Where(x => x.Id == id).FirstOrDefaultAsync();

				if (account == null)
					return new AccountBalanceResponse { Message = "Account not found." };


				List<Balance> queryResult = await _context.Balances
					.Include(x => x.Account)
					.Where(x => x.AccountId == id)
					.ToListAsync();

				if (queryResult == null || !queryResult.Any())
					return new AccountBalanceResponse { Message = "No balances found." };

				response.IsFound = true;
				response.Balances = GetBalances(queryResult);

				return response;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetBalanceByAccountId: {ex.Message}");
				return null;
			}
		}

		public async Task<AccountBalanceResponse> GetAllBalancesByMonth(int year, int month)
		{
			try
			{
				AccountBalanceResponse response = new AccountBalanceResponse();

				List<Balance> queryResult = await _context.Balances
					.Include(x => x.Account)
					.Where(x => x.FinancialYear == year && x.TransactionMonth == month)
					.ToListAsync();

				if (queryResult == null || !queryResult.Any())
					return new AccountBalanceResponse { Message = "No balances found." };

				response.IsFound = true;
				response.Balances = GetBalances(queryResult);

				return response;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetAllBalancesByMonth: {ex.Message}");
				return null;
			}
		}

		public async Task<AccountBalanceResponse> GetAllBalancesByYear(int year)
		{
			try
			{
				AccountBalanceResponse response = new AccountBalanceResponse();

				List<Balance> queryResult = await _context.Balances
					.Include(x => x.Account)
					.Where(x => x.FinancialYear == year)
					.ToListAsync();

				if (queryResult == null || !queryResult.Any())
					return new AccountBalanceResponse { Message = "No balances found." };

				response.IsFound = true;
				response.Balances = GetBalances(queryResult);

				return response;
			}
			catch (Exception ex)
			{
				_logger.LogError($"Something went wrong in GetAllBalancesByYear: {ex.Message}");
				return null;
			}
		}

		#endregion

		#region Private Methods

		private List<BalanceDto> GetBalances(List<Balance> balanceRecords)
		{
			List<BalanceDto> balances = new List<BalanceDto>();

			foreach (Balance balance in balanceRecords)
			{
				balances.Add(new BalanceDto 
				{ 
					Account = balance.Account.AccName, 
					Month = balance.TransactionMonth,
					Year = balance.FinancialYear,
					Balance = balance.Amount 
				});
			}

			return balances;
		}

		#endregion

	}
}
