﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;

namespace AccountManagementAPI.Services
{
	public interface IAccountBalanceService
	{
		Task<AccountCreationResponse> CreateAccount(string name);
		Task<List<Account>> GetAccounts();
		Task<AccountBalanceResponse> GetBalancesByAccountId(int id);
		Task<AccountBalanceResponse> GetAccountBalancesByYear(int accountId, int year);
		Task<AccountBalanceResponse> GetAccountBalancesByMonth(int accountId, int year, int month);
		Task<AccountBalanceResponse> GetAllBalancesByMonth(int year, int month);
		Task<AccountBalanceResponse> GetAllBalancesByYear(int year);
	}
}
