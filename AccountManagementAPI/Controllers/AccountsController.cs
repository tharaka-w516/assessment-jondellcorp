﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto;
using AccountManagementAPI.Data.Dto.RequestDto;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using AccountManagementAPI.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AccountManagementAPI.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class AccountsController : ControllerBase
	{
		#region Private Variables

		private readonly IAccountBalanceService _accountBalanceService;
		private readonly IFileUploadService _fileUploadService;

		#endregion

		#region Constructor

		public AccountsController(IAccountBalanceService accountBalanceService, IFileUploadService fileUploadService)
		{
			_accountBalanceService = accountBalanceService;
			_fileUploadService = fileUploadService;
		}

		#endregion

		#region Public Methods

		[HttpPost]
		[ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> Create([FromForm] FileUploadRequest uploadRequest)
		{
			if (uploadRequest == null)
				return BadRequest("Upload request is null.");

			List<ValidationResult> validationResults = new List<ValidationResult>();
			bool isValid = Validator.TryValidateObject(uploadRequest, new ValidationContext(uploadRequest), validationResults, true);

			if (!isValid)
				return BadRequest(validationResults);

			string fileContentType = uploadRequest.AccountsFile.ContentType;
			string modifiedFileName = Utility.GetFileName(uploadRequest.AccountsFile.FileName, uploadRequest.TransactionMonth, uploadRequest.FinancialYear);
			string uploadResult;

			using (Stream stream = uploadRequest.AccountsFile.OpenReadStream())
			{
				uploadResult = await _fileUploadService.Create(stream, modifiedFileName, fileContentType);
			}

			if (string.IsNullOrEmpty(uploadResult))
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong in File Upload.");

			return Ok(uploadResult);
		}

		[HttpPost("create")]
		[ProducesResponseType(typeof(Account), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> CreateAccount([FromBody] AccountCreationRequest creationRequest)
		{
			if (creationRequest == null)
				return BadRequest("Missing required parameters.");

			if (string.IsNullOrEmpty(creationRequest.Name))
				return BadRequest("Missing required parameters.");

			AccountCreationResponse result = await _accountBalanceService.CreateAccount(creationRequest.Name);

			if (!result.IsCreated)
				return StatusCode(StatusCodes.Status500InternalServerError, result.Message);

			return Ok(result.Account);
		}

		[HttpGet]
		[ProducesResponseType(typeof(List<Account>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetAll()
		{
			List<Account> result = await _accountBalanceService.GetAccounts();

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong while retrieving account details.");

			if (!result.Any())
				return NotFound("No accounts found.");

			return Ok(result);
		}

		[HttpGet("balance/{id}/year/{year}/month/{month}")]
		[ProducesResponseType(typeof(List<BalanceDto>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetBalancesByMonth(int id, int year, int month)
		{
			if (id == 0 || year == 0 || month == 0)
				return BadRequest("Required parameters are missing.");

			var result = await _accountBalanceService.GetAccountBalancesByMonth(id, year, month);

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");

			if (!result.IsFound)
				return NotFound(result.Message);

			return Ok(result.Balances);

		}

		[HttpGet("balance/{id}/year/{year}")]
		[ProducesResponseType(typeof(List<BalanceDto>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetBalancesByYear(int id, int year)
		{
			if (id == 0 || id == year)
				return BadRequest("Required parameters are missing.");

			var result = await _accountBalanceService.GetAccountBalancesByYear(id, year);

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");

			if (!result.IsFound)
				return NotFound(result.Message);

			return Ok(result.Balances);
		}

		[HttpGet("balance/{id}")]
		[ProducesResponseType(typeof(List<BalanceDto>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetBalance(int id)
		{
			if (id == 0)
				return BadRequest("Account Id parameter is missing.");

			var result = await _accountBalanceService.GetBalancesByAccountId(id);

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");

			if (!result.IsFound)
				return NotFound(result.Message);

			return Ok(result.Balances);
		}

		[HttpGet("balance/year/{year}")]
		[ProducesResponseType(typeof(List<BalanceDto>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetAllBalancesByYear(int year)
		{
			if (year == 0)
				return BadRequest("Year not found.");

			var result = await _accountBalanceService.GetAllBalancesByYear(year);

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");

			if (!result.IsFound)
				return NotFound(result.Message);

			return Ok(result.Balances);
		}

		[HttpGet("balance/month/{month}/year/{year}")]
		[ProducesResponseType(typeof(List<BalanceDto>), StatusCodes.Status200OK)]
		[ProducesResponseType(StatusCodes.Status400BadRequest)]
		[ProducesResponseType(StatusCodes.Status404NotFound)]
		[ProducesResponseType(StatusCodes.Status500InternalServerError)]
		public async Task<IActionResult> GetAllBalancesByMonth(int year, int month)
		{
			if (year == 0 || month == 0)
				return BadRequest("Required parameters are missing.");

			var result = await _accountBalanceService.GetAllBalancesByMonth(year, month);

			if (result == null)
				return StatusCode(StatusCodes.Status500InternalServerError, "Something went wrong!");

			if (!result.IsFound)
				return NotFound(result.Message);

			return Ok(result.Balances);
		}

		#endregion
	}
}
