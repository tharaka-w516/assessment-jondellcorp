﻿using AccountManagementAPI.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace AccountManagementAPI.Data
{
	public class AccountContext : DbContext
	{
		public AccountContext(DbContextOptions<AccountContext> options) : base(options)
		{
		}

		public AccountContext()
		{
		}

		public DbSet<Account> Accounts { get; set; }
		public DbSet<Balance> Balances { get; set; }

		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Account>().ToTable("Account");
			modelBuilder.Entity<Balance>().ToTable("Balance");
		}
	}
}
