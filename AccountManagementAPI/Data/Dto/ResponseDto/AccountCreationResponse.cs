﻿using AccountManagementAPI.Data.Models;

namespace AccountManagementAPI.Data.Dto.ResponseDto
{
	public class AccountCreationResponse
	{
		public bool IsCreated { get; set; }
		public string Message { get; set; }
		public Account Account { get; set; }
	}
}
