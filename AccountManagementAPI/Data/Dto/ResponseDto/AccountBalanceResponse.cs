﻿using System.Collections.Generic;

namespace AccountManagementAPI.Data.Dto.ResponseDto
{
	public class AccountBalanceResponse
	{
		public List<BalanceDto> Balances { get; set; }
		public bool IsFound { get; set; }
		public string Message { get; set; }
	}
}
