﻿namespace AccountManagementAPI.Data.Dto.ResponseDto
{
	public class RecordCreationResponse
	{
		public bool IsCreated { get; set; }
		public string Message { get; set; }
	}
}
