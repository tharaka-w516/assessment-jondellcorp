﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace AccountManagementAPI.Data.Dto.RequestDto
{
	public class FileUploadRequest
	{
		[Required(ErrorMessage = "File not found.")]
		public IFormFile AccountsFile { get; set; }
		[Required(ErrorMessage = "Month parameter not found.")]
		public int TransactionMonth { get; set; }
		[Required(ErrorMessage = "Year parameter not found.")]
		public int FinancialYear { get; set; }
	}
}
