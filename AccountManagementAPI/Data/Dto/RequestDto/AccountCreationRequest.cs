﻿namespace AccountManagementAPI.Data.Dto.RequestDto
{
	public class AccountCreationRequest
	{
		public string Name { get; set; }
	}
}
