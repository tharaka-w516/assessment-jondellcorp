﻿namespace AccountManagementAPI.Data.Dto
{
	public class BalanceDto
	{
		public string Account { get; set; }
		public decimal Balance { get; set; }
		public int Month { get; set; }
		public int Year { get; set; }
	}
}
