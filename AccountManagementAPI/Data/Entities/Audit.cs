﻿using System;

namespace AccountManagementAPI.Data.Entities
{
	public class Audit
	{
		public DateTime CreatedDate { get; set; }
		public string CreatedBy { get; set; }
		public DateTime ModifiedDate { get; set; }
		public string ModifiedBy { get; set; }
	}
}
