﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using AccountManagementAPI.Data.Entities;

namespace AccountManagementAPI.Data.Models
{
	public class Account : Audit
	{
		[Key]
		public int Id { get; set; }
		public string AccName { get; set; }
		public ICollection<Balance> Balance { get; set; }
	}
}
