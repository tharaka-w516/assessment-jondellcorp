﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace AccountManagementAPI.Data.Models
{
	public class Balance
	{
		[Key]
		public int Id { get; set; }
		public int AccountId { get; set; }
		[Column(TypeName = "decimal(15,2)")]
		public decimal Amount { get; set; }
		public int TransactionMonth { get; set; }
		public int FinancialYear { get; set; }
		public Account Account { get; set; }
	}
}
