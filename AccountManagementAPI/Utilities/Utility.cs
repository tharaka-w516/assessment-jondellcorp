﻿using System;
using System.IO;

namespace AccountManagementAPI.Utilities
{
	public static class Utility
	{
		#region Private Variables

		private const string FILENAME_SEPARATOR = "_";
		private const string FILENAME_FORMAT = "{0}_{1}_{2}";

		#endregion

		#region Public Methods

		public static string GetFileName(string fileName, int month, int year)
		{
			try
			{
				string fileExtenstion = Path.GetExtension(fileName);
				string fileNameWithoutExtension = Path.GetFileNameWithoutExtension(fileName);

				return $"{string.Format(FILENAME_FORMAT, fileNameWithoutExtension, Convert.ToString(year), Convert.ToString(month))}{fileExtenstion}";
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}

		#endregion
	}
}
