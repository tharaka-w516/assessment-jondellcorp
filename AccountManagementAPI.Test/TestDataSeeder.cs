﻿using AccountManagementAPI.Data;
using AccountManagementAPI.Data.Models;

namespace AccountManagementAPI.Test
{
	public static class TestDataSeeder
	{
		#region Public Methods

		public static void SeedData(AccountContext accountContext)
		{
			SeedAccounts(accountContext);
			SeedAccountBalances(accountContext);

			accountContext.SaveChanges();
		}

		#endregion

		#region Private Methods

		private static void SeedAccounts(AccountContext accountContext)
		{
			accountContext.Accounts.Add(
				new Account { AccName = "R&D" }
			);

			accountContext.Accounts.Add(
				new Account { AccName = "Canteen" }
			);

			accountContext.Accounts.Add(
				new Account { AccName = "CEO's car" }
			);

			accountContext.Accounts.Add(
				new Account { AccName = "Marketing" }
			);

			accountContext.Accounts.Add(
				new Account { AccName = "Parking Fines" }
			);
		}

		private static void SeedAccountBalances(AccountContext accountContext)
		{
			accountContext.Balances.Add(
				new Balance { AccountId = 1, Amount = 5.63M, FinancialYear = 2020, TransactionMonth = 1 }
			);

			accountContext.Balances.Add(
				new Balance { AccountId = 2, Amount = 50000M, FinancialYear = 2020, TransactionMonth = 1 }
			);

			accountContext.Balances.Add(
				new Balance { AccountId = 3, Amount = 10000M, FinancialYear = 2020, TransactionMonth = 1 }
			);

			accountContext.Balances.Add(
				new Balance { AccountId = 4, Amount = -600M, FinancialYear = 2020, TransactionMonth = 1 }
			);

			accountContext.Balances.Add(
				new Balance { AccountId = 5, Amount = 2000M, FinancialYear = 2020, TransactionMonth = 1 }
			);
		}

		#endregion
	}
}
