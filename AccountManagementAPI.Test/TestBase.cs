﻿using System;
using AccountManagementAPI.Data;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;

namespace AccountManagementAPI.Test
{
	public class TestBase
	{
		#region Public Variables

		public AccountContext context;

		#endregion

		#region Private Variables

		private SqliteConnection connection;

		#endregion

		#region Constructor

		public TestBase()
		{
			context = GetTestAccountContext();
		}

		#endregion

		#region Public Methods

		public AccountContext GetTestAccountContext()
		{
			connection = new SqliteConnection("DataSource=:memory:");
			connection.Open();

			var options = new DbContextOptionsBuilder<AccountContext>()
				.UseSqlite(connection)
				.Options;

			var context = new AccountContext(options);

			if (context.Database.EnsureCreated())
			{
				TestDataSeeder.SeedData(context);
				return context;
			}

			throw new Exception("Failed to create database.");
		}

		#endregion
	}
}
