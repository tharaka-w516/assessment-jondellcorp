﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetAccountsTest : AccountBalanceServiceTestBase
	{
		#region Private Variables

		private readonly List<Account> _testResponse;

		#endregion

		#region Constructor

		public GetAccountsTest()
		{
			_testResponse = GetTestAccounts();
		}

		#endregion

		#region Test Methods

		[Fact]
		public async Task GetAccounts_Success()
		{
			var result = await mockAccountBalanceService.GetAccounts();

			Assert.IsType<List<Account>>(result);
			Assert.True(result.Any());
			Assert.Equal(_testResponse.Count, result.Count);
		}

		[Fact]
		public async Task GetAccounts_DbSetReturnNull_ReturnNull()
		{
			DbSet<Account> nullResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullResult);

			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccounts();

			Assert.Null(result);
		}

		[Fact]
		public async Task GetAccounts_DbException_ReturnsNull()
		{
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccounts();

			Assert.Null(result);
		}

		#endregion
	}
}
