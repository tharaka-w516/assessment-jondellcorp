﻿using System;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetBalancesByAccountIdTest : AccountBalanceServiceTestBase
	{
		#region Private Variables

		private readonly AccountBalanceResponse _testResponse;

		#endregion

		#region Constructor

		public GetBalancesByAccountIdTest()
		{
			_testResponse = GetTestBalancesForAccount();
		}

		#endregion

		#region Test Methods

		[Theory]
		[InlineData(1)]
		public async Task GetBalancesByAccountId_Success(int accountId)
		{
			var result = await mockAccountBalanceService.GetBalancesByAccountId(accountId);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.True(result.IsFound);
			Assert.Equal(_testResponse.Balances[0].Balance, result.Balances[0].Balance);
		}

		[Theory]
		[InlineData(6)]
		[InlineData(default(int))]
		public async Task GetBalancesByAccountId_InvalidAccountId_ReturnsIsFoundFalse(int accountId)
		{
			var result = await mockAccountBalanceService.GetBalancesByAccountId(accountId);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.False(result.IsFound);
		}

		[Theory]
		[InlineData(1)]
		public async Task GetBalancesByAccountId_DbSetReturnsNull_ReturnsNull(int accountId)
		{
			DbSet<Account> nullResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullResult);
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetBalancesByAccountId(accountId);

			Assert.Null(result);
		}

		[Theory]
		[InlineData(1)]
		public async Task GetBalancesByAccountId_DBException_ReturnsNull(int accountId)
		{
			mockDbContext.Setup(x => x.Set<Account>()).Throws(new Exception());
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetBalancesByAccountId(accountId);

			Assert.Null(result);
		}

		#endregion
	}
}
