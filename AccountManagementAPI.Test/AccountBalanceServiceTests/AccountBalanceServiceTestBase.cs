﻿using System.Collections.Generic;
using AccountManagementAPI.Data;
using AccountManagementAPI.Data.Dto;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Moq;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class AccountBalanceServiceTestBase : TestBase
	{
		#region Public Variables

		public IAccountBalanceService mockAccountBalanceService;
		public Mock<AccountContext> mockDbContext;
		public Mock<DbSet<Account>> mockSet;
		public Mock<ILogger<AccountBalanceService>> mockLogger;

		#endregion

		#region Constructor

		public AccountBalanceServiceTestBase()
		{
			mockDbContext = new Mock<AccountContext>();
			mockLogger = new Mock<ILogger<AccountBalanceService>>();
			mockAccountBalanceService = new AccountBalanceService(context, mockLogger.Object);
		}

		#endregion

		#region Helper Methods

		public AccountBalanceResponse GetTestBalanceResponse()
		{
			List<BalanceDto> balances = new List<BalanceDto>()
			{
				new BalanceDto
				{
					Account = "R&D",
					Balance = 5.63M,
					Month = 1,
					Year = 2020
				}
			};

			return new AccountBalanceResponse
			{
				Balances = balances,
				IsFound = true
			};
		}

		public List<Account> GetTestAccounts()
		{
			return new List<Account>
			{
				new Account { AccName = "R&D" },
				new Account { AccName = "Canteen" },
				new Account { AccName = "CEO's car" },
				new Account { AccName = "Marketing" },
				new Account { AccName = "Parking Fines" }
			};
		}

		public AccountBalanceResponse GetTestBalancesForAccount()
		{
			var balances = new List<BalanceDto>
			{
				new BalanceDto { Account = "R&D", Balance = 5.63M, Month = 1, Year = 2021}
			};

			return new AccountBalanceResponse
			{
				IsFound = true,
				Balances = balances
			};
		}

		#endregion
	}
}
