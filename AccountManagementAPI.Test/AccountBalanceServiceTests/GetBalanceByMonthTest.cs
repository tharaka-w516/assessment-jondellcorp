﻿using System;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetBalanceByMonthTest : AccountBalanceServiceTestBase
	{
		#region Private Variables

		private AccountBalanceResponse _testResponse;

		#endregion

		#region Constructor

		public GetBalanceByMonthTest()
		{
			_testResponse = GetTestBalanceResponse();
		}

		#endregion

		#region Test Methods

		[Theory]
		[InlineData(1, 1, 2020)]
		public async Task GetAccountBalancesByMonth_Success(int accountId, int month, int year)
		{
			var result = await mockAccountBalanceService.GetAccountBalancesByMonth(accountId, year, month);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.True(result.IsFound);
			Assert.Equal(_testResponse.Balances[0].Account, result.Balances[0].Account);
			Assert.Equal(_testResponse.Balances[0].Balance, result.Balances[0].Balance);
		}

		[Theory]
		[InlineData(100, 2020, 1)]
		public async Task GetAccountBalancesByMonth_InvalidAccountId_ReturnsNullBalance(int accountId, int year, int month)
		{
			var result = await mockAccountBalanceService.GetAccountBalancesByMonth(accountId, year, month);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.False(result.IsFound);
			Assert.Null(result.Balances);
		}

		[Theory]
		[InlineData(1, 2021, 1)]
		[InlineData(1, 2020, 50)]
		public async Task GetAccountBalancesByMonth_InvalidParameters_ReturnsIsFoundFalse(int accountId, int year, int month)
		{
			var result = await mockAccountBalanceService.GetAccountBalancesByMonth(accountId, year, month);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.False(result.IsFound);
			Assert.Null(result.Balances);
		}

		[Theory]
		[InlineData(1, 1, 2020)]
		public async Task GetAccountBalancesByMonth_AccountsDbSetIsNull_ReturnsNull (int accountId, int month, int year)
		{
			DbSet<Account> nullResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullResult);
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccountBalancesByMonth(accountId, year, month);

			Assert.Null(result);
		}

		[Theory]
		[InlineData(1, 1, 2020)]
		public async Task GetAccountBalancesByMonth_DbException_ReturnsNull(int accountId, int month, int year)
		{
			mockDbContext.Setup(x => x.Set<Account>()).Throws(new Exception());
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccountBalancesByMonth(accountId, year, month);

			Assert.Null(result);
		}

		#endregion
	}
}
