﻿using System;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetBalanceByYearTest : AccountBalanceServiceTestBase
	{
		#region Private Variables

		private readonly AccountBalanceResponse _testResponse;

		#endregion

		#region Constructor

		public GetBalanceByYearTest()
		{
			_testResponse = GetTestBalanceResponse();
		}

		#endregion

		#region Test Methods

		[Theory]
		[InlineData(1, 2020)]
		public async Task GetAccountBalancesByYear_Success(int accountId, int year)
		{
			var result = await mockAccountBalanceService.GetAccountBalancesByYear(accountId, year);

			Assert.True(result.IsFound);
			Assert.Equal(_testResponse.Balances.Count, result.Balances.Count);
			Assert.Equal(_testResponse.Balances[0].Balance, result.Balances[0].Balance);
		}

		[Theory]
		[InlineData(0, 0)]
		[InlineData(1, 0)]
		[InlineData(0, 2020)]
		[InlineData(5, 1999)]
		public async Task GetAccountBalancesByYear_InvalidParameters_ReturnsIsFoundFalse(int accountId, int year)
		{
			var result = await mockAccountBalanceService.GetAccountBalancesByYear(accountId, year);

			Assert.False(result.IsFound);
		}

		[Theory]
		[InlineData(1, 2020)]
		public async Task GetAccountBalancesByYear_AccountsDbSetIsNull_ReturnsNull(int accountId, int year)
		{
			DbSet<Account> nullResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullResult);
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccountBalancesByYear(accountId, year);

			Assert.Null(result);
		}

		[Theory]
		[InlineData(1, 2020)]
		public async Task GetAccountBalancesByYear_DbException_ReturnsNull(int accountId, int year)
		{
			mockDbContext.Setup(x => x.Set<Account>()).Throws(new Exception());
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAccountBalancesByYear(accountId, year);

			Assert.Null(result);
		}

		#endregion
	}
}
