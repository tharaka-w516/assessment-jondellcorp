﻿using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Services;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class CreateAccountTest : AccountBalanceServiceTestBase
	{
		#region Test Methods

		[Theory]
		[InlineData("Canteen")]
		public async Task CreateAccount_ReturnsAccountCreationResponse_Success(string name)
		{
			var result = await mockAccountBalanceService.CreateAccount(name);

			Assert.IsType<AccountCreationResponse>(result);
			Assert.True(result.IsCreated);
			Assert.Equal(name, result.Account.AccName);
		}

		[Theory]
		[InlineData("")]
		public async Task CreateAccount_EmptyAccountName_ReturnsNotCreatedResponse(string name)
		{
			var result = await mockAccountBalanceService.CreateAccount(name);

			Assert.IsType<AccountCreationResponse>(result);
			Assert.False(result.IsCreated);
		}

		[Theory]
		[InlineData("Canteen")]
		public async Task CreateAccount_DbException_ReturnsIsCreatedFalse(string name)
		{
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.CreateAccount(name);

			Assert.IsType<AccountCreationResponse>(result);
			Assert.False(result.IsCreated);
		}

		#endregion
	}
}
