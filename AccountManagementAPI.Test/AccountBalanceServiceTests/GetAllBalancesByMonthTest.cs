﻿using System;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetAllBalancesByMonthTest : AccountBalanceServiceTestBase
	{
		#region Test Methods

		[Theory]
		[InlineData(2020, 1)]
		public async Task GetAllBalancesByMonth_Success(int year, int month)
		{
			var result = await mockAccountBalanceService.GetAllBalancesByMonth(year, month);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.True(result.IsFound);
		}

		[Theory]
		[InlineData(2019, 5)]
		[InlineData(0, 1)]
		[InlineData(2020, 0)]
		public async Task GetAllBalancesByMonth_InvalidParameters_ReturnsIsFoundFalse(int year, int month)
		{
			var result = await mockAccountBalanceService.GetAllBalancesByMonth(year, month);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.False(result.IsFound);
		}

		[Theory]
		[InlineData(2021, 1)]
		public async Task GetAllBalancesByMonth_DbResultNull_ReturnsNull(int year, int month)
		{
			DbSet<Account> nullAccountsResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullAccountsResult);
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAllBalancesByMonth(year, month);

			Assert.Null(result);
		}

		[Theory]
		[InlineData(2021, 1)]
		public async Task GetAllBalancesByMonth_DbException_ReturnsNull(int year, int month)
		{
			mockDbContext.Setup(x => x.Set<Balance>()).Throws(new Exception());
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAllBalancesByMonth(year, month);

			Assert.Null(result);
		}

		#endregion
	}
}
