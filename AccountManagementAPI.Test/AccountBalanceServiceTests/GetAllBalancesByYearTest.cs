﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AccountManagementAPI.Data.Dto.ResponseDto;
using AccountManagementAPI.Data.Models;
using AccountManagementAPI.Services;
using Microsoft.EntityFrameworkCore;
using Xunit;

namespace AccountManagementAPI.Test.AccountBalanceServiceTests
{
	public class GetAllBalancesByYearTest : AccountBalanceServiceTestBase
	{
		#region Test Methods

		[Theory]
		[InlineData(2020)]
		public async Task GetAllBalancesByYear_Success(int year)
		{
			var result = await mockAccountBalanceService.GetAllBalancesByYear(year);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.True(result.IsFound);
			Assert.True(result.Balances.Any());
		}

		[Theory]
		[InlineData(1990)]
		[InlineData(0)]
		public async Task GetAllBalancesByYear_InvalidYear_ReturnsIsFoundFalse(int year)
		{
			var result = await mockAccountBalanceService.GetAllBalancesByYear(year);

			Assert.IsType<AccountBalanceResponse>(result);
			Assert.False(result.IsFound);
			Assert.Null(result.Balances);
		}

		[Theory]
		[InlineData(2020)]
		public async Task GetAllBalancesByYear_DbSetReturnsNull_ReturnsNull(int year)
		{
			DbSet<Account> nullResult = null;
			mockDbContext.Setup(x => x.Set<Account>()).Returns(nullResult);
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAllBalancesByYear(year);

			Assert.Null(result);;
		}

		[Theory]
		[InlineData(2020)]
		public async Task GetAllBalancesByYear_DBException_ReturnsNull(int year)
		{
			mockDbContext.Setup(x => x.Set<Balance>()).Throws(new Exception());
			mockAccountBalanceService = new AccountBalanceService(mockDbContext.Object, mockLogger.Object);

			var result = await mockAccountBalanceService.GetAllBalancesByYear(year);

			Assert.Null(result);
		}

		#endregion
	}
}
